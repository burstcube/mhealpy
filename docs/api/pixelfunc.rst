Pixelization functions
======================

These functions can be call without referencing any class. e.g.::

  >>> import mhealpy as mhp
  >>> mhp.nest2uniq(nside = 128, ipix = 3)
  65539


Single-resolution maps
----------------------
      
.. automodule:: mhealpy.pixelfunc.single
   :members:
   :undoc-members:
   :special-members:

Multi-resolution maps
---------------------

.. automodule:: mhealpy.pixelfunc.moc
   :members:
   :undoc-members:
   :special-members:

