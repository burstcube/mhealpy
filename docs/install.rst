Installation
============

Run::
  
  pip install mhealpy

Or alternatively, install it from source::

  pip install --user git+https://gitlab.com/burstcube/mhealpy.git@master

For developers
--------------

First, install `healpy`, the only dependency::

  pip install healpy

Then you can get a working version of `mhealpy` with::
  
  git clone git@gitlab.com:burstcube/mhealpy.git
  cd  mhealpy
  python setup.py develop
