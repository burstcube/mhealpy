from mhealpy import HealpixBase
import mhealpy as mhp

import healpy as hp

import numpy as np
from numpy import array_equal as arr_eq

from astropy.visualization.wcsaxes import WCSAxes

import pytest
from pytest import approx

import matplotlib
matplotlib.use('agg')

def test_init_info():

    # mesh valid
    # npix
    # order
    # nside
    # scheme
    # is nested (moc and nested)
    # is ring
    # is_moc

    # Ring, default
    nside = 64
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(nside = nside)

    assert m.nside == nside
    assert m.npix == npix
    assert m.order == order
    assert m.scheme == 'RING'
    assert m.is_mesh_valid()
    assert m.is_ring
    assert not m.is_nested
    assert not m.is_moc

    # Nested
    nside = 128
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(nside = nside, scheme = 'nested')

    assert m.nside == nside
    assert m.npix == npix
    assert m.order == order
    assert m.scheme == 'NESTED'
    assert m.is_mesh_valid()
    assert not m.is_ring
    assert m.is_nested
    assert not m.is_moc

    # MOC
    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15]
    m = HealpixBase(uniq)

    assert m.nside == 4
    assert m.npix == len(uniq)
    assert m.order == 2
    assert m.scheme == 'NUNIQ'
    assert m.is_mesh_valid()
    assert not m.is_ring
    assert m.is_nested
    assert m.is_moc

    # Order doesn't matter
    uniq =     [64, 65, 5,6,7,8,9,10,11, 19, 12,13,14,15, 66, 67, 17, 18]
    m = HealpixBase(uniq)

    assert m.nside == 4
    assert m.npix == len(uniq)
    assert m.order == 2
    assert m.scheme == 'NUNIQ'
    assert m.is_mesh_valid()
    assert not m.is_ring
    assert m.is_nested
    assert m.is_moc

    
    # Bad MOC.
    m = HealpixBase([   65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15])
    assert not m.is_mesh_valid() 

    m = HealpixBase([64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14  ])
    assert not m.is_mesh_valid() 

    m = HealpixBase([64, 65, 66, 67, 17, 18, 19, 5,6,7,  9,10,11,12,13,14,15])
    assert not m.is_mesh_valid() 

    m = HealpixBase([64, 65, 66, 67, 16, 17, 18, 19, 5,6,7,  9,10,11,12,13,14,15])
    assert not m.is_mesh_valid() 

def test_pixfunc():

    # Ring, default
    nside = 128
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(order = order)

    for pix in [0, npix-1, int(npix/2), np.array([0, npix-1, int(npix/2)])]:

        x,y,z = m.pix2vec(pix)
        assert arr_eq((x,y,z), hp.pix2vec(nside, pix))
        assert arr_eq(m.vec2pix(x,y,z), hp.vec2pix(nside, x,y,z))

        theta,phi = m.pix2ang(pix)
        assert arr_eq((theta,phi), hp.pix2ang(nside, pix))

        lon,lat = m.pix2ang(pix, lonlat = True)
        assert lon == approx(np.rad2deg(phi))
        assert lat == approx(90-np.rad2deg(theta))

        assert arr_eq(m.ang2pix(theta,phi), hp.ang2pix(nside, theta, phi))

        assert arr_eq(m.ang2pix(theta,phi), m.ang2pix(lon,lat, lonlat = True))
        
        assert arr_eq(m.pix2uniq(pix), 4*nside*nside + hp.ring2nest(nside, pix))

        start = hp.ring2nest(nside, pix)
        assert arr_eq(m.pix2range(nside, pix), (start, start+1))
        assert arr_eq(m.pix2range(nside*2, pix), (start*4, (start+1)*4))

        assert arr_eq(m.nest2pix(pix), hp.nest2ring(nside, pix))
        
    rs = m.pix_rangesets(nside)
    start = hp.ring2nest(nside, range(npix))
    assert arr_eq(rs.start, start)
    assert arr_eq(rs.stop, start + 1)

    rs = m.pix_rangesets(4*nside)
    assert arr_eq(rs.start, start*16)
    assert arr_eq(rs.stop, (start + 1)*16)

    assert m.pixarea().value == approx(4*np.pi/12/nside/nside)
    
    # Nested
    nside = 32
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(order = order, scheme = 'nested')

    for pix in [0, npix-1, int(npix/2), np.array([0, npix-1, int(npix/2)])]:

        x,y,z = m.pix2vec(pix)
        assert arr_eq((x,y,z), hp.pix2vec(nside, pix, nest=True))
        assert arr_eq(m.vec2pix(x,y,z), hp.vec2pix(nside, x,y,z, nest=True))

        theta,phi = m.pix2ang(pix)
        assert arr_eq((theta,phi), hp.pix2ang(nside, pix, nest=True))
        assert arr_eq(m.ang2pix(theta,phi), hp.ang2pix(nside, theta, phi, nest=True))
        
        assert arr_eq(m.pix2uniq(pix), 4*nside*nside + pix)

        start = pix
        assert arr_eq(m.pix2range(nside, pix), (start, start+1))
        assert arr_eq(m.pix2range(nside*2, pix), (start*4, (start+1)*4))

        assert arr_eq(m.nest2pix(pix), pix)

    rs = m.pix_rangesets(nside)
    start = np.array(range(npix))
    assert arr_eq(rs.start, start)
    assert arr_eq(rs.stop, start + 1)

    rs = m.pix_rangesets(4*nside)
    assert arr_eq(rs.start, start*16)
    assert arr_eq(rs.stop, (start + 1)*16)

    assert m.pixarea().value == approx(4*np.pi/12/nside/nside)

    # MOC
    order =    np.array([2,   2, 0,0,0,0,0, 0, 0,  1,  0, 0, 0, 0,  2,  2,  1,  1])
    uniq =     np.array([64, 65, 5,6,7,8,9,10,11, 19, 12,13,14,15, 66, 67, 17, 18])
    m = HealpixBase(uniq)

    nside = hp.order2nside(order)
    nest_pix = uniq - 4*nside*nside
    pix = np.array(range(m.npix))
    
    x,y,z = m.pix2vec(pix)
    assert arr_eq((x,y,z),
                  hp.pix2vec(nside, nest_pix, nest=True))
    assert arr_eq(mhp.uniq2nest(uniq[m.vec2pix(x,y,z)])[1],
                  hp.vec2pix(nside, x,y,z, nest=True))
    
    theta,phi = m.pix2ang(pix)
    assert arr_eq((theta,phi),
                  hp.pix2ang(nside, nest_pix, nest=True))
    assert arr_eq(mhp.uniq2nest(uniq[m.ang2pix(theta,phi)])[1],
                  hp.ang2pix(nside, theta, phi, nest=True))
    
    assert arr_eq(m.pix2uniq(pix), uniq)
    
    start = nest_pix
    assert arr_eq(m.pix2range(nside, pix), (start, start+1))
    assert arr_eq(m.pix2range(2*nside, pix), (start*4, (start+1)*4))
    
    rs = m.pix_rangesets(m.nside)
    start = nest_pix * (m.nside*m.nside//nside//nside)
    stop = (nest_pix+1) * (m.nside*m.nside//nside//nside)
    assert arr_eq(rs.start, start)
    assert arr_eq(rs.stop, stop)

    rs = m.pix_rangesets(mhp.MAX_NSIDE)
    start = nest_pix * (mhp.MAX_NSIDE*mhp.MAX_NSIDE//nside//nside)
    stop = (nest_pix+1) * (mhp.MAX_NSIDE*mhp.MAX_NSIDE//nside//nside)
    assert arr_eq(rs.start, start)
    assert arr_eq(rs.stop, stop)

    assert m.pixarea(pix).value == approx(4*np.pi/12/nside/nside)


    uniq =     np.array([64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15])
    m = HealpixBase(uniq)

    assert arr_eq(m.nest2pix(range(hp.nside2npix(m.nside))),
                  [ 0,  1,  2,  3,
                    4,  4,  4,  4,  5,  5,  5,  5,  6,  6,  6,  6,
                    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
                    8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
                    9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
                    10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 
                    11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 
                    12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 
                    13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 
                    14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 
                    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 
                    16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 
                    17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17])
    
def test_conformable():

    m1 = HealpixBase(order = 2, scheme = 'ring')
    m2 = HealpixBase(order = 3, scheme = 'ring')
    m3 = HealpixBase(order = 3, scheme = 'nested')
    m4 = HealpixBase(order = 3, scheme = 'nested')
    m5 = HealpixBase([64, 65, 5,6,7,8,9,10,11, 19, 12,13,14,15, 66, 67, 17, 18])
    m6 = HealpixBase([64, 65, 5,6,7,8,9,10,11, 19, 12,13,14,15, 66, 67, 17, 18])
    m7 = HealpixBase([65, 64, 5,6,7,8,9,10,11, 19, 12,13,14,15, 66, 67, 17, 18])

    assert m1 != m2
    assert m2 != m3
    assert m3 == m4
    assert m5 == m6
    assert m6 != m7
    
def test_adaptive_mesh():

    m = HealpixBase.moc_from_pixels(4, [0,5], nest = False)

    uniq = [16, 17, 18, 76, 77, 78, 79, 5,6, 7, 8,9,10,11,12,13,14,15]
    mGood = HealpixBase(uniq)

    m.moc_sort()
    mGood.moc_sort()
    
    assert m == mGood
    
def test_interp():

    # Ring, default
    nside = 64
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(nside = nside)

    theta = np.linspace(0, np.pi, 10)
    phi = np.linspace(0, 2*np.pi, 10)
    
    for t,p in zip(theta, phi):
        assert arr_eq(m.get_interp_weights(t, p),
                      hp.get_interp_weights(nside, t, p))

        lon,lat = np.rad2deg(p), 90-np.rad2deg(t)

        for w1,w2 in zip(m.get_interp_weights(lon, lat, lonlat = True),
                         hp.get_interp_weights(nside, t, p)):
            assert  w1 == approx(w2)
        
        
    assert arr_eq(m.get_interp_weights(theta, phi),
                      hp.get_interp_weights(nside, theta, phi))
    
    # Nested
    nside = 64
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(nside = nside, scheme = 'nested')

    for t,p in zip(theta, phi):
        assert arr_eq(m.get_interp_weights(t, p),
                      hp.get_interp_weights(nside, t, p, nest = True))

    assert arr_eq(m.get_interp_weights(theta, phi),
                      hp.get_interp_weights(nside, theta, phi, nest = True))

    # MOC
    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15]
    m = HealpixBase(uniq)

    for t,p in zip(theta, phi):
        pix,weights = hp.get_interp_weights(m.nside, t, p, nest = True)
        assert arr_eq(m.get_interp_weights(t, p),
                      (m.nest2pix(pix), weights))

    pix,weights = hp.get_interp_weights(m.nside, theta, phi, nest = True)
    assert arr_eq(m.get_interp_weights(theta, phi),
                      (m.nest2pix(pix), weights))
    
def test_query_pixels():

    # Neighbohrs, disc, strip, polygon, boundaries

    # Ring, default
    nside = 16
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(nside = nside)

    for pix in range(m.npix):
        assert arr_eq(m.get_all_neighbours(pix),
                      hp.get_all_neighbours(nside, pix))

        lon,lat = m.pix2ang(pix, lonlat = True)
        assert arr_eq(m.get_all_neighbours(pix),
                      m.get_all_neighbours(lon, lat, lonlat = True))
        
        theta,phi = m.pix2ang(pix)
        assert arr_eq(m.get_all_neighbours(pix),
                      m.get_all_neighbours(theta, phi))

    theta = np.linspace(0, np.pi, 100)
    phi = np.linspace(0, 2*np.pi, 100)
    vec = hp.ang2vec(theta, phi)
    np.random.seed(0)
    radius = np.random.uniform(0, 2*np.pi, 100)
    
    for v,r in zip(vec,radius):
        assert arr_eq(m.query_disc(v, r),
                      hp.query_disc(nside, v, r))
    
    # Nested
    nside = 16
    npix = hp.nside2npix(nside)
    order = hp.nside2order(nside)
    m = HealpixBase(nside = nside, scheme = 'nested')

    for pix in range(m.npix):
        assert arr_eq(m.get_all_neighbours(pix),
                      hp.get_all_neighbours(nside, pix, nest = True))
    
    theta = np.linspace(0, np.pi, 100)
    phi = np.linspace(0, 2*np.pi, 100)
    vec = hp.ang2vec(theta, phi)
    np.random.seed(0)
    radius = np.random.uniform(0, 2*np.pi, 100)
    
    for v,r in zip(vec,radius):
        assert arr_eq(m.query_disc(v, r),
                      hp.query_disc(nside, v, r, nest = True))
    
    # MOC
    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15]
    m = HealpixBase(uniq)

    for pix in range(m.npix):

        neighbohrs = m.get_all_neighbours(pix)

        assert arr_eq(neighbohrs,
                      m.nest2pix(hp.get_all_neighbours(m.nside, *m.pix2ang(pix), nest = True)))
    
    theta = np.linspace(0, np.pi, 100)
    phi = np.linspace(0, 2*np.pi, 100)
    vec = hp.ang2vec(theta, phi)
    np.random.seed(0)
    radius = np.random.uniform(0, 2*np.pi, 100)
    
    for v,r in zip(vec,radius):

        pixels = m.query_disc(v, r, inclusive = True)
        
        test_pixels = []

        for p in range(m.npix):

            nside, pix = mhp.uniq2nest(m.pix2uniq(p))

            if pix in hp.query_disc(nside, v, r, nest = True, inclusive = True):
                test_pixels += [p]

        assert arr_eq(pixels, test_pixels)

def test_plot():

    # This only tests that there are no exeception
    # generating the plot, not that the plots
    # was drawn correctly

    uniq =     [64, 65, 66, 67, 17, 18, 19, 5,6,7,8,9,10,11,12,13,14,15]
    m = HealpixBase(uniq)

    m.plot_grid()
    m.plot_grid('mollview')
    m.plot_grid('orthview')
    m.plot_grid('cartview')
    plot, ax = m.plot_grid('orthview', linestyle = ':')

    assert isinstance(plot[0], matplotlib.lines.Line2D)
    assert isinstance(ax, WCSAxes)
    
    m.plot_grid(ax)
